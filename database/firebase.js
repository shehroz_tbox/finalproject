// database/firebaseDb.js

import * as firebase from 'firebase';

const firebaseConfig = {
    firebase_url: "https://mark-00001.firebaseio.com",
    storage_bucket: "mark-00001.appspot.com",
    apiKey: "AIzaSyDGUfOyQM-QGWinlXhURLFLho1JQpqa49A",
    projectId: "mark-00001",
    appId: "1:209154371143:android:c14ab69ab356a19c094b62:ios:e5c010481d99e503094b62",
    messagingSenderId: "209154371143"
};

firebase.initializeApp(firebaseConfig);

export default firebase;