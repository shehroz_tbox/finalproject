import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { StyleSheet } from "react-native"

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#003f5c',
        alignItems: 'center',
        justifyContent: 'center',
      },
      logo:{
        fontWeight:"bold",
        fontSize:hp(7.5),
        color:"#fb5b5a",
        marginBottom:hp(5)
      },
      inputView:{
        width: wp(80),
        backgroundColor:"#465881",
        borderRadius:wp(25),
        height:hp(5.5),
        marginBottom:hp(2),
        justifyContent:"center",
        padding:hp(2),
      },
      dashboardContainer: {
        flexDirection: "column"
      },
      inputText:{
        height:hp(5),
        color:"white"
      },
      forgot:{
        color:"white",
        fontSize:11
      },
      loginBtn:{
        width: wp(80),
        backgroundColor:"#fb5b5a",
        borderRadius:hp(25),
        height:hp(5.5),
        alignItems:"center",
        justifyContent:"center",
        marginTop:hp(1),
        marginBottom:hp(4)
      },
      logoutBtn:{
        position: 'absolute',
        bottom:wp(5),
        borderColor:'#fb5b5a',
        borderWidth:wp(0.3),
        width: wp(80),
        // backgroundColor:"#fb5b5a",
        borderRadius:hp(25),
        height:hp(5.5),
        alignItems:"center",
        justifyContent:"center",
        marginTop:hp(1),
        marginBottom:hp(0)
      },
      updateBtn:{
        // position: 'absolute',
        // bottom:wp(5),
        borderColor:'#fb5b5a',
        borderWidth:wp(0.3),
        width: wp(80),
        // backgroundColor:"#fb5b5a",
        borderRadius:hp(25),
        height:hp(5.5),
        alignItems:"center",
        justifyContent:"center",
        // marginTop:hp(1),
        marginBottom:hp(1)
      },
      loginText:{
        color:"white"
      },
      textStyle: {
        fontSize: hp(1.5),
        alignSelf:"flex-start",
        color: "white",
        marginLeft: wp(5),
        marginBottom: hp(2)
      },
      scene: {
        flex: 1,
      },
}); 