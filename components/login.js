import styles from '../styles/styles'
import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert, ActivityIndicator, TouchableOpacity } from 'react-native';
import firebase from '../database/firebase';


export default class Login extends Component {
  
  constructor() {
    super();
    this.state = { 
      email: '', 
      password: '',
      isLoading: false
    }
  }

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  userLogin = () => {
    if(this.state.email === '' && this.state.password === '') {
      Alert.alert('Enter details to signin!')
    } else {
      this.setState({
        isLoading: true,
      })
      firebase
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then((res) => {
        console.log(res)
        console.log('User logged-in successfully!')
        this.setState({
          isLoading: false,
          email: '', 
          password: ''
        })
        this.props.navigation.navigate('Dashboard')
      })
      .catch(error => this.setState({ errorMessage: error.message }))
    }
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }    
    return (
      // <View style={styles.container}>  
      //   <TextInput
      //     style={styles.inputStyle}
      //     placeholder="Email"
      //     value={this.state.email}
      //     onChangeText={(val) => this.updateInputVal(val, 'email')}
      //   />
      //   <TextInput
      //     style={styles.inputStyle}
      //     placeholder="Password"
      //     value={this.state.password}
      //     onChangeText={(val) => this.updateInputVal(val, 'password')}
      //     maxLength={15}
      //     secureTextEntry={true}
      //   />   
      //   <Button
      //     color="#3740FE"
      //     title="Signin"
      //     onPress={() => this.userLogin()}
      //   />   

      //   <Text 
      //     style={styles.loginText}
      //     onPress={() => this.props.navigation.navigate('Signup')}>
      //     Don't have account? Click here to signup
      //   </Text>                          
      // </View>
      <View style={styles.container}>
        <Text style={styles.logo}>e-Hifz</Text>
        <View style={styles.inputView} >
          <TextInput  
            style={styles.inputText}
            value={this.state.email}
            selectionColor={"#fb5b5a"}
            placeholder="Email..." 
            placeholderTextColor="white"
            onChangeText={(val) => this.updateInputVal(val, 'email')}
            />
        </View>
        <View style={styles.inputView} >
          <TextInput  
            secureTextEntry
            selectionColor={"#fb5b5a"}
            style={styles.inputText}
            value={this.state.password}
            placeholder="Password..." 
            placeholderTextColor="white"
            onChangeText={(val) => this.updateInputVal(val, 'password')}
            />
        </View>
        {/* <TouchableOpacity>
          <Text style={styles.forgot}>Forgot Password?</Text>
        </TouchableOpacity> */}
        <TouchableOpacity 
          onPress={() => this.userLogin()}
          style={styles.loginBtn}
          >
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
        <TouchableOpacity>
          {/* <Text style={styles.loginText}>Signup</Text> */}
          <Text 
            style={styles.loginText}
            onPress={() => this.props.navigation.navigate('Signup')}>
            Don't have account? Click here to signup
          </Text> 
        </TouchableOpacity>

  
      </View>
    );
  }
}
