import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert, ActivityIndicator, TouchableOpacity } from 'react-native';
import firebase from '../database/firebase';
import styles from '../styles/styles'
import { useNavigation } from '@react-navigation/native';

export default class UpdateInfo extends Component {
  
  constructor() {
    super();
    this.state = { 
      displayName: firebase.auth().currentUser.displayName,
      email: firebase.auth().currentUser.email, 
      isLoading: false
    }
  }

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  signOut = () => {
    firebase.auth().signOut().then(() => {
      this.props.navigation.navigate('Login')
    })
    .catch(error => this.setState({ errorMessage: error.message }))
  }

    deleleAccount = () => {
      firebase.auth().currentUser.delete().then(() => {
        console.log("The user was deleted successfully")
        this.props.navigation.navigate('Signup')
      }).catch(function (error) {
        console.error({error})
      })
    }

    updateInfo = () => {
      if(this.state.email.trim() === '' || this.state.displayName.trim() === '' ) {
        Alert.alert('No field can be blank!')
      } else {
        var user = firebase.auth().currentUser;
        user.updateProfile({
          displayName: this.state.displayName,
        }).then(function() {
          console.log("The name was updated successfully")
        }).catch(function(error) {
          console.error({error})
        }) && 
        user.updateEmail(this.state.email).then(() => {
          console.log("The email was updated successfully")
          // this.props.navigation.navigate('Login')
          Alert.alert("The profile was updated successfully!")
        }).catch(function (error) {
          console.error({error})
        })
      }
    }
  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }    
    
    return (
      <View style={styles.container}>
        <Text style={styles.logo}>e-Hifz</Text>
        <View style={styles.inputView}>
        <TextInput
          style={styles.inputText}
          value={this.state.displayName}
          placeholder="Name"
          selectionColor="#fb5b5a"
          placeholderTextColor="white"
          onChangeText={(val) => this.updateInputVal(val, 'displayName')}
        />  
        </View>
        <View style={styles.inputView} >
          <TextInput  
            selectionColor="#fb5b5a"
            style={styles.inputText}
            value={this.state.email}
            placeholder="Email..." 
            placeholderTextColor="white"
            onChangeText={(val) => this.updateInputVal(val, 'email')}
            />
        </View>
        <TouchableOpacity
          style={styles.updateBtn}
          // onPress={() => console.log("The display name is: " + this.state.displayName)}
          onPress={() => this.updateInfo()}
          onPress={() => 
            Alert.alert(
              'Edit Profile',
              'Update your profile?',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel'
                },
                { text: 'OK', onPress: () => this.updateInfo()}
              ],
              { cancelable: false }
            )        
          }
        >
          <Text style={styles.loginText}>UPDATE INFO</Text>
        </TouchableOpacity>
        <TouchableOpacity 
          style={styles.loginBtn}
          onPress={() => 
            Alert.alert(
              'Account Deletion Warning',
              'Are you sure you want to delete your account?',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel'
                },
                { text: 'OK', onPress: () => 
                this.deleleAccount() && this.props.navigation.navigate('Signup'),
              }
              ],
              { cancelable: false }
            )        
          }
          >
          <Text style={styles.loginText}>DELETE ACCOUNT</Text>
        </TouchableOpacity>
        <TouchableOpacity 
          style={styles.logoutBtn}
          onPress={() => 
            Alert.alert(
              'Logout Confirmation',
              'Are you sure you want to Logout?',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel'
                },
                { text: 'OK', onPress: () => this.signOut()}
              ],
              { cancelable: false }
            )        
          }          >
          <Text style={styles.loginText}>LOGOUT</Text>
        </TouchableOpacity>
      </View>

    );
  }
}
