// // // components/dashboard.js

// import React, { Component } from 'react';
// import { StyleSheet, View, Text, Button } from 'react-native';
// import firebase from '../database/firebase';

// export default class Dashboard extends Component {
//   constructor() {
//     super();
//     this.state = { 
//       uid: ''
//     }
//   }

//   signOut = () => {
//     firebase.auth().signOut().then(() => {
//       this.props.navigation.navigate('Login')
//     })
//     .catch(error => this.setState({ errorMessage: error.message }))
//   }  

//   render() {
//     this.state = { 
//       displayName: firebase.auth().currentUser.displayName,
//       uid: firebase.auth().currentUser.uid
//     }    
//     return (
//       <View style={styles.container}>
//         <Text style = {styles.textStyle}>
//           Hello, {this.state.displayName}
//         </Text>

//         <Button
//           color="#3740FE"
//           title="Logout"
//           onPress={() => this.signOut()}
//         />
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     display: "flex",
//     justifyContent: 'center',
//     alignItems: 'center',
//     padding: 35,
//     backgroundColor: '#fff'
//   },
//   textStyle: {
//     fontSize: 15,
//     marginBottom: 20
//   }
// });


import * as React from 'react';
import { View, StyleSheet, Dimensions, StatusBar, Text } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import styles from '../styles/styles';
import AllProfile from './allProfile';
import AddProfile from './addProfile';
import UpdateInfo from './updateInfo';
import Login from './login';

const FirstRoute = () => (
  <View style={[styles.scene, { backgroundColor: '#ff4081' }]} />
);

const SecondRoute = () => (
  <View style={[styles.scene, { backgroundColor: '#673ab7' }]} />
);

const ThirdRoute = () => (
  <View style={[styles.scene, { backgroundColor: '#fe7401' }]} />
);
 
const initialLayout = { width: Dimensions.get('window').width };

export default function Dashboard({navigation}) {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
        { key: 'first', title: 'ALL PROFILE'  },
        { key: 'second', title: 'ADD PROFILE' },
        { key: 'third', title: 'UPDATE INFO' },
      ]);
  const renderScene = ({ route }) => {
    switch (route.key) {
      case 'first':
        return <AllProfile navigation={navigation}/>;
      case 'second':
        return <AddProfile navigation={navigation}/>;
      case 'third':
        return <UpdateInfo navigation={navigation}/>
      default:
        return null;
    }
  }; 

  return (
    <TabView
      navigationState={{ index, routes }}
      renderTabBar={props => (
        <TabBar
          {...props}
          renderLabel={({ route, color }) => (
            <Text style={{ color: '#fb5b5a', fontWeight: "bold" }}>
              {route.title}
            </Text>
          )}
          style={{backgroundColor: '#003f5c'}}
        />
      )}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
      style={styles.dashboardContainer}
    />
  );
}
