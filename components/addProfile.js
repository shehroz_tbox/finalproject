import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert, ActivityIndicator, TouchableOpacity } from 'react-native';
import firebase from '../database/firebase';
import styles from '../styles/styles'

export default class AddProfile extends Component {
  
  constructor() {
    super();
    this.state = { 
      displayName: '',
      email: '', 
      password: '',
      isLoading: false
    }
  }
  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }
  signOut = () => {
    firebase.auth().signOut().then(() => {
      this.props.navigation.navigate('Login')
    })
    .catch(error => this.setState({ errorMessage: error.message }))
  }
  registerUser = () => {
    if(this.state.email.trim() === '' || this.state.password.trim() === '' || this.state.displayName.trim() === '' ) {
      Alert.alert('Enter details to register!')
    } else {
      this.setState({
        isLoading: true,
      })
      firebase
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then((res) => {
        res.user.updateProfile({
          displayName: this.state.displayName
        })
        Alert.alert("The student was registered successfully!")
        console.log('User registered successfully!')
        this.setState({
          isLoading: false,
          displayName: '',
          email: '', 
          password: ''
        })
      })
      .catch(error => this.setState({ errorMessage: error.message }))      
    }
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }    
    return (
      <View style={styles.container}>
        <Text style={styles.logo}>e-Hifz</Text>
        <View style={styles.inputView}>
        <TextInput
          style={styles.inputText}
          value={this.state.displayName}
          placeholder="Name"
          placeholderTextColor="white"
          onChangeText={(val) => this.updateInputVal(val, 'displayName')}
        />  
        </View>
        <View style={styles.inputView} >
          <TextInput  
            style={styles.inputText}
            value={this.state.email}
            placeholder="Email..." 
            placeholderTextColor="white"
            onChangeText={(val) => this.updateInputVal(val, 'email')}
            />
        </View>
        <View style={styles.inputView} >
          <TextInput  
            secureTextEntry
            style={styles.inputText}
            value={this.state.password}
            placeholder="Password..." 
            placeholderTextColor="white"
            onChangeText={(val) => this.updateInputVal(val, 'password')}
            />
        </View>
        <TouchableOpacity 
          style={styles.loginBtn}
          onPress={() => this.registerUser()}
          >
          <Text style={styles.loginText}>REGISTER STUDENT</Text>
        </TouchableOpacity>
        <TouchableOpacity 
          style={styles.logoutBtn}
          onPress={() => 
            Alert.alert(
              'Logout Confirmation',
              'Are you sure you want to Logout?',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel'
                },
                { text: 'OK', onPress: () => this.signOut()}
              ],
              { cancelable: false }
            )        
          }               >
          <Text style={styles.loginText}>LOGOUT</Text>
        </TouchableOpacity>
      </View>

    );
  }
}
